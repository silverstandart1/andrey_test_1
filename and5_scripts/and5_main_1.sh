#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script for Custer deployment / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Update cluster\n"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 -o "StrictHostKeyChecking no" "ls -l"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 "pwd"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 "env"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 "cat /etc/os-release"

ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 -o "StrictHostKeyChecking no" "cd ~; rm -rf ~/DEVHOME; ls -l;"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 -o "StrictHostKeyChecking no" "cd ~; mkdir -p \$PWD/DEVHOME; mkdir -p \$PWD/DEVHOME/11; cd \$PWD/DEVHOME/11; ls -l; echo \$PWD;"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 -o "StrictHostKeyChecking no" "git clone https://gitlab.com/silverstandart1/andrey_test_1.git \$PWD/DEVHOME/11; cd \$PWD/DEVHOME/11; ls -l; echo \$PWD;"

ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 -o "StrictHostKeyChecking no" "cat \$PWD/DEVHOME/11/and5_scripts/and5_svr_runner.sh;"
ssh -i ~/DEV_HOME/0_SSH/infodba infodba@51.250.1.13 -o "StrictHostKeyChecking no" "chmod 777 \$PWD/DEVHOME/11/and5_scripts/and5_svr_runner.sh; \$PWD/DEVHOME/11/and5_scripts/and5_svr_runner.sh;"


