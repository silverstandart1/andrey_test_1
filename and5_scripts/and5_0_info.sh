#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script prints info about cluster / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

helm list --all-namespaces
echo -e "\n"
/home/infodba/yandex-cloud/bin/yc managed-kubernetes node-group list
/home/infodba/yandex-cloud/bin/yc compute instance list
kubectl get nodes -o wide
echo -e "\n"
kubectl get pods --all-namespaces
echo -e "\n"
kubectl get services -o wide --all-namespaces
echo -e "\n"
kubectl get pod -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --all-namespaces
echo -e "\n"
