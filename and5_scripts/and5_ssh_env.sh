#!/bin/sh
# ---------------------------------------------------------------------------------------
# Pre script for Custer deployment / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Update SSH Agent\n"
command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Copy Keys\n"
mkdir -p ~/DEV_HOME/0_SSH
chmod 700 ~/DEV_HOME/0_SSH
cp $AND5_INFODBA_SSH_PRIVATE_KEY ~/DEV_HOME/0_SSH/infodba
chmod 700 ~/DEV_HOME/0_SSH/infodba

#echo -e "\n"
#echo -e "-------------------------------------------------------------------------------- Yandex Console installation\n"
#curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
#/home/infodba/yandex-cloud/bin/yc --help
#cd /home/infodba/yandex-cloud/bin
#ll

